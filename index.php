<?php

require __DIR__ . '/vendor/autoload.php';

use App\BotDesRognons;
use App\Helpers\DotEnv;

(new DotEnv(__DIR__ . '/.env'))->load();
$botDesRognons = new BotDesRognons();
$botDesRognons->exec();
