<?php

namespace App;

use Monolog\Handler\StreamHandler;
use App\Services\DatabaseService;
use App\Services\TwitterService;
use App\Helpers\SafeTweet;
use Monolog\Logger;

/**
 * Class BotDesRognons
 *
 * @package App
 * @author Logan DESSE <logan.desse@gmail.com>
 */
class BotDesRognons
{
    /** @var DatabaseService $databaseService */
    private $databaseService;

    /** @var TwitterService $twitterService */
    private $twitterService;

    /** @var Logger $logger */
    private $logger;

    /**
     * BotDesRognons constructor.
     */
    public function __construct()
    {
        $this->databaseService = new DatabaseService();
        $this->twitterService = new TwitterService();

        $this->logger = new Logger('botrognons-logger');
        $this->logger->pushHandler(new StreamHandler('var/log/botrognons.log', Logger::DEBUG));
    }

    /**
     * Main function. Post tweet.
     */
    function exec(): void
    {
        $result = $this->databaseService->getNextWord();
        $tweet = $result['word'] . " des rognons";

        if(SafeTweet::isSafe($tweet) && $result['ban'] !== 1) {
            if(getenv("APP_ENV") === "prod") {
                if($this->twitterService->postTweet($tweet)) {
                    $this->logger->info('New tweet post!', ['tweet' => $tweet]);
                    $this->databaseService->updateWord($result['id']);
                }
            } else {
                print($tweet);
            }
        }
    }
}
