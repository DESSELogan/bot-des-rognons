<?php

namespace App\Services;

use PDO;

/**
 * Class DatabaseService
 *
 * @package App\Services
 * @author Logan DESSE <logan.desse@gmail.com>
 */
class DatabaseService
{
    /** @var PDO $pdo */
    private $pdo;

    /**
     * DatabaseService constructor.
     */
    public function __construct()
    {
        $this->pdo = new PDO(
            'mysql:host='.getenv('DATABASE_HOST').';dbname='.getenv('DATABASE_NAME'),
            getenv('DATABASE_LOGIN'),
            getenv('DATABASE_PASSWORD'),
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING)
        );
        $this->pdo->exec("SET CHARACTER SET utf8");
    }

    /**
     * Get the next word in the list.
     *
     * @return array result of the query
     */
    public function getNextWord(): array
    {
        $query = "
            SELECT * 
            FROM dictionary 
            WHERE tweet != 1 
            LIMIT 1;
        ";
        $response = $this->pdo->query($query);
        $result = $response->fetch();
        $response->closeCursor();

        return $result;
    }

    /**
     * Update the last post word in the database.
     *
     * @param int $id the id of the last word post.
     */
    public function updateWord(int $id): void
    {
        $query = "
            UPDATE dictionary
            SET tweet = 1
            WHERE id = {$id}
        ";
        $response = $this->pdo->query($query);
        $response->closeCursor();
    }
}
