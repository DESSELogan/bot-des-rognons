<?php

namespace App\Services;

use Monolog\Handler\StreamHandler;
use TwitterAPIExchange;
use Monolog\Logger;
use Exception;

/**
 * Class TwitterService
 *
 * @package App\Services
 * @author Logan DESSE <logan.desse@gmail.com>
 */
class TwitterService
{
    /** @var TwitterAPIExchange $twitterAPI */
    private $twitterAPI;

    /** @var Logger $logger */
    private $logger;

    /**
     * TwitterService constructor.
     */
    public function __construct()
    {
        $this->twitterAPI = new TwitterAPIExchange([
            'oauth_access_token' => getenv('API_TOKEN'),
            'oauth_access_token_secret' => getenv('API_SECRET_TOKEN'),
            'consumer_key' => getenv('API_CONSUMER_KEY'),
            'consumer_secret' => getenv('API_CONSUMER_SECRET_KEY')
        ]);

        $this->logger = new Logger('botrognons-logger');
        $this->logger->pushHandler(new StreamHandler('var/log/botrognons.log', Logger::DEBUG));
    }

    /**
     * Post a tweet
     *
     * @param string $tweet the tweet to post.
     *
     * @return bool if the tweet is post or not.
     */
    public function postTweet(string $tweet): bool
    {
        $url = "https://api.twitter.com/1.1/statuses/update.json";
        $body = array('status' =>  $tweet);
        $method = "POST";

        try {
            $this->twitterAPI
                ->buildOauth($url, $method)
                ->setPostfields($body)
                ->performRequest();

            return true;
        } catch (Exception $e) {
            $this->logger->error($e, ['tweet' => $tweet]);
        }

        return false;
    }
}
