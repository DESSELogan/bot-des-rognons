# Bot des Rognons

Le bot des rognons est un bot développé par [Logan DESSE](https://gitlab.com/DESSELogan) pour twitter qui va simplement
poster un tweet toutes les deux heures. 

Le compte du bot est accessible [ici](https://twitter.com/BotRognons).

## Remerciement
* Ce bot est inspiré du ["BotDuCul" de WhiteFangs](https://github.com/WhiteFangs/BotDuCul) merci à lui aussi pour le [SafeTweet](https://github.com/WhiteFangs/SafeTweet).

* [TwitterAPiExchange](http://github.com/j7mbo/twitter-api-php)
* Merci aussi au vrillage de donner de si belle idée.

## Mise en place
### prérequis
* php 7.1 minimumm
* composer
* mariaDB

### clone et installation

Le fichier d'import de la base de données est disponible dans les sources (database.sql)

```bash
git clone https://gitlab.com/DESSELogan/bot-des-rognons.git
```
```bash
composer install
```
```bash
cp .env.dist .env
```
Vous devez ensuite remplir les champs du .env avec vos propres informations
vous pouvez vous referer à la doc de [TwitterAPiExchange](http://github.com/j7mbo/twitter-api-php)

